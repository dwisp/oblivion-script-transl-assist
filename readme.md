# Oblivion Script Translation Assistant

This is a simple python script for managing oblivion mod script translations.


## Motivation

TES CS's 'export script messageboxes' functionality doesn't allow to export all the strings from mod scripts.
Non-message strings are pretty common in mods with dynamic name generation, model path matching, etc.

This results in an inconvenience where one has to manually search and update those strings.
It's also possible to accidentally modify something and break the script while doing so.

This script allows to extract ALL of the strings from mod's scripts, and put back in their translated versions.
It's also possible to update already existing translations using scripts of a newer version of a mod.

Afterwards the user can the copy-paste and compile those scripts in TES CS or import them through other means.
If you are aware of a less tedious way to import+compile scripts back into the ESP file, let me know.

## Main ideas

* versioning scripts and translations with git
* only keep scripts and translations to avoid discrepancies in translations vs. translated scripts
* translation files are jsons with translated string underneath the original for better readability

### Setting up

You will need **git** and **python**, preferrably >= 3.7 for this to work.
Place the scripts of a mod you wish to translate into `./scripts/your_mod_name/orig/`.

### Exporting strings and translating

Export does write all of the scripts' strings into json files with empty translations.

1. Run `python osta -a export -n your_mod_name`
2. The blank translation files shall appear in `./translations/your_mod_name/` in form of json files
3. Open them one-by-one and fill in the blank translation rows, then save the file. Keep in mind that leaving a blank string equals "do not translate this" rather than "replace this with an empty string"
4. run `git add ./translations/your_mod_name` and `git commit -m "something about this translation"`
5. Now you have a mod + translation version saved in git

### Importing translations

Import reads available translations, imports them into original scripts and outputs them in a special folder.

1. Run `python osta -a import -n your_mod_name`
2. The translated scripts will appear in `./scripts/your_mod_name/tran/`


### Updating existing translations

Update merges latest translations with a newer versions of the original scripts.

1. Run `python osta -a update -n your_mod_name`
2. Your translations in `./translations/your_mod_name/` will be updated with any new strings, and the strings no longer in use will be removed
3. Fill in translations for the new strings and save the files
4. Run `git add ./translations/your_mod_name` and `git commit -m "something about this translation update"`

### Generate translation from translated scripts

Match creates new translation files out of original + translated scripts.
Not that scripts versions must match since strings are matched positionally, e.g. first string in the original script maps to a first string in translated script, etc

1. Place existing original scripts in `./scripts/your_mod_name/orig/`
2. Place existing translated scripts in `./scripts/your_mod_name/tran/`
3. Run `python osta -a match -n your_mod_name`
4. Run `git add ./translations/your_mod_name` and `git commit -m "something about this translation update"`