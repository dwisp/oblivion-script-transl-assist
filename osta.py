import argparse
import json
import os
import re
from os.path import exists, join
from typing import Dict, List

ENC = 'utf-8'
DIR_SCRIPTS = '.\\scripts'
DIR_TRANSLATION = '.\\translations'
DIR_ORIG = 'orig'
DIR_TRAN = 'tran'

RE_STRING = '|'.join(('"[^"\n]*"', "'[^'\n]*'"))
EXPORT, IMPORT, UPDATE, MATCH = 'export', 'import', 'update', 'match'

parser = argparse.ArgumentParser()
parser.add_argument(
    '-a',
    '--action',
    type=str,
    choices=[EXPORT, IMPORT, UPDATE, MATCH]
)
parser.add_argument(
    '-n',
    '--mod_name',
    type=str
)
args = parser.parse_args()

path_scripts_orig = join(DIR_SCRIPTS, args.mod_name, DIR_ORIG)
path_scripts_tran = join(DIR_SCRIPTS, args.mod_name, DIR_TRAN)
path_translation = join(DIR_TRANSLATION, args.mod_name)


def create_dir(path: str) -> None:
    if not os.path.exists(path):
        os.makedirs(path)


def read_script(path: str) -> str:
        with open(path, 'r', encoding=ENC) as f:
            script = f.read()
        return script


def write_script(path: str, script: str) -> None:
    with open(path, 'w', encoding=ENC) as f:
        f.write(script)


def read_translation_json(path: str) -> Dict[str, str]:
    translation = {}
    if exists(path):
        with open(path, 'r', encoding=ENC) as f:
            translation = json.load(f)
    return translation


def write_translation_json(path: str, translation: Dict) -> None:
    """
    Writes a json with formatting convenient for translation

    Args:
        path (str): file to write in
        translation (Dict): translation dictionary {orig: tran}
    """
    if translation:
        formatted_list = [k + ':\n' + v for k, v in translation.items()]
        translation_n = ',\n'.join(formatted_list)
        with open(path, 'w', encoding=ENC) as f:
            f.write('{\n')
            f.write(translation_n)
            f.write('\n}')


def export_strings(script: str) -> List[str]:
    """
    Finds all the strings in a script
    If nothing is found, returns an empty List

    Args:
        script (str): script to search strings in

    Returns:
        List[str]: extracted strings
    """
    match = re.findall(RE_STRING, script)
    strings = []
    if match:
        strings = list(match)
    return strings


def update_translation(translation: Dict[str, str], matches: List[str]) -> Dict[str, str]:
    """
    Updates the translation with possible new untranslated strings
    Removes strings that are not present anymore

    Args:
        translation (Dict[str, str]): current translation
        matches (List[str]): matches found in a newer version of a script

    Returns:
        Dict[str, str]: updated translation
    """
    match_dict = {k: '""' for k in matches}
    # translating already translated strings, new ones remain blank
    updated_translation = {**match_dict, **translation}
    # only leaving the strings which exist in the latest version
    updated_translation = {
        orig: tran for orig, tran
        in updated_translation.items()
        if orig in matches
    }
    return updated_translation


def import_strings(script: str, translation: Dict[str, str]) -> str:
    """
    Replaces original strings with translated versions

    Args:
        script (str): a script to translate
        translation (Dict[str, str]): translation

    Returns:
        str: translated script
    """
    for orig, tran in translation.items():
        # "" is considered to be "nothing to translate here"
        if tran:
            # only replace quoted versions
            orig_sq = f"'{tran}'"
            orig_dq = f'"{tran}"'
            script = script.replace(orig_sq, tran)
            script = script.replace(orig_dq, tran)
    return script


if __name__ == '__main__':

    create_dir(path_scripts_orig)
    create_dir(path_scripts_tran)
    create_dir(path_translation)

    for script_filename in os.listdir(path_scripts_orig):
        path_script_orig = join(path_scripts_orig, script_filename)
        path_script_tran = join(path_scripts_tran, script_filename)
        path_script_translation = join(path_translation, script_filename)

        script = read_script(path_script_orig)
        
        if args.action == EXPORT:
            matches = export_strings(script)
            empty_translation = {k: '""' for k in matches}
            write_translation_json(path_script_translation, empty_translation)
        elif args.action == IMPORT:
            translation = read_translation_json(path_script_translation)
            script = import_strings(script, translation)
            # write only scripts with something to translate
            if translation:
                write_script(path_script_tran, script)
        elif args.action == UPDATE:
            translation = read_translation_json(path_script_translation)
            translation = update_translation(translation, matches)
            write_translation_json(path_script_translation, translation)
        elif args.action == MATCH:
            if exists(path_script_tran):
                script_tran = read_script(path_script_tran)
                matches_orig = export_strings(script)
                matches_tran = export_strings(script_tran)
                translation = dict(zip(matches_orig, matches_tran))
                write_translation_json(path_script_translation, translation)
